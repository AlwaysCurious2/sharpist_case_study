import pandas as pd

pd.set_option('display.max_columns', 220)
pd.set_option('display.max_rows', 500)
pd.set_option('display.width', 1000)

data = pd.read_csv("data.csv")
data.rename(columns={'Bankrupt?': 'target'}, inplace=True)
response_rate = data.target.sum() / data.target.count()

data.columns

# for col in data.columns:
#     data.hist(column=col)

for i in data.columns:
    for j in data.columns:
        if i != j:
            if sum(data[i] - data[j]) == 0:
                print(i, j)

data.groupby('target').count().reset_index()