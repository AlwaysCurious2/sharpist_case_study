# Sharpist Data Scientist Project

## Background
A Logistic Regression, Random Forest and Gradient Boosting Machine were used.
Each of these three were trained on the dataset as well as an upsampled version of the data.
This upsampling was done to counter the drastically unbalanced classes.

## Getting started
data.csv is found in the main directory.
model.py is the main python file to run.
utils.py contains helper functions.
Other files are work in progress and can be ignored.

## Running the program
Run the code in models.py