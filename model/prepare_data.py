import pandas as pd
# from model.utils import plot_pvo
# import matplotlib.pyplot as plt
# import shap
# import numpy as np
# import os

data = pd.read_csv("data.csv")
data.rename(columns={'Bankrupt?': 'target'}, inplace=True)
data.sort_values(by='target', inplace=True)
data.reset_index(inplace=True)

# upsample
data_1 = data.loc[data.target == 1, :]
data_upsampled = pd.concat([data, data_1, data_1, data_1, data_1], axis=0)


# feature_importance = pd.DataFrame(
#     {'feature': feature_columns, 'importance': rf.feature_importances_})
# feature_importance = feature_importance.sort_values('importance', ascending=False)
# top_feature_importance = feature_importance.iloc[0, 1] * 1.0
# feature_importance.importance /= top_feature_importance
# print(feature_importance)


# train_x['quantile'] = round(train_x['predicted'].rank(pct=True), 2)
# test_x['quantile'] = round(test_x['predicted'].rank(pct=True), 2)
# train_x['quantile_logistic'] = round(train_x['predicted_logistic'].rank(pct=True), 2)
# test_x['quantile_logistic'] = round(test_x['predicted_logistic'].rank(pct=True), 2)
#
# train_pvo = (
#     train_x
#         .loc[:, ['predicted', response, 'quantile']]
#         .groupby('quantile',
#                  as_index=False
#                  )
#         .mean()
# )
#
# test_pvo = (
#     test_x
#         .loc[:, ['predicted', response, 'quantile']]
#         .groupby('quantile',
#                  as_index=False
#                  )
#         .mean()
# )
#
#
# train_pvo_logistic = (
#     train_x
#         .loc[:, ['predicted_logistic', response, 'quantile_logistic']]
#         .groupby('quantile_logistic',
#                  as_index=False
#                  )
#         .mean()
# )
#
# test_pvo_logistic = (
#     test_x
#         .loc[:, ['predicted_logistic', response, 'quantile_logistic']]
#         .groupby('quantile_logistic',
#                  as_index=False
#                  )
#         .mean()
# )
#
# train_pvo_fig = plot_pvo(
#     data=train_pvo,
#     title='PVO train',
#     actual_col=response,
#     pred_col='predicted',
#     percentile_col='quantile'
# )
#
# validation_pvo_fig = plot_pvo(
#     data=test_pvo,
#     title='PVO validation',
#     actual_col=response,
#     pred_col='predicted',
#     percentile_col='quantile'
# )
#
# ## Shapley Values
#
# sample = np.random.randint(0, train_x.shape[0], 5000)
#
# explainer = shap.TreeExplainer(rf)
# shap_values = explainer.shap_values(train_x[feature_columns].iloc[sample])
#
# if not os.path.exists('./diagnostics/shap/'):
#     os.makedirs('./diagnostics/shap/')
#
# fig = shap.summary_plot(
#     shap_values=shap_values,
#     features=train_x[feature_columns].iloc[sample],
#     show=False,
#     plot_size=(18, 12)
# )

# plt.tight_layout()

#
# # predictions_prob = data['prediction']
# for sample_x, sample_y in [(train_x, train_y), (test_x, test_y)]:
#     predictions_prob = sample_x['predicted']
#     false_positive_rate, recall, thresholds = roc_curve(sample_y, predictions_prob)
#     roc_auc = auc(false_positive_rate, recall)
#     plt.plot(false_positive_rate, recall, 'g', label='AUC %s = %0.2f' % ('model name', roc_auc))
#     plt.plot([0, 1], [0, 1], 'r--')
#     # plt.legend(loc = 'lower right')
#     plt.ylabel('True Positive Rate')
#     plt.xlabel('False Positive Rate')
#     plt.title('ROC Curve')