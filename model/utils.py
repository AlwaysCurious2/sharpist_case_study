# import matplotlib.pyplot as plt

#
# def plot_pvo(
#         data,
#         title="PvO: overall",
#         percentile_col="ntile",
#         actual_col="response",
#         pred_col="predictions",
#         log_scale=False
# ):
#     data = data.sort_values(percentile_col, ascending=True)
#     fig = plt.figure(figsize=(12, 8))
#     plt.clf()
#
#     percentile = data[percentile_col]
#     avg_predicted = data[pred_col]
#     avg_actual = data[actual_col]
#
#     # plt.subplot2grid((3, 1), (0, 0), rowspan=2)
#     plt.plot(percentile, avg_predicted, lw=3, label="Predicted")
#     plt.plot(percentile, avg_actual, lw=3, label="Observed")
#     plt.legend(loc="upper left")
#     plt.ylabel("Average Score")
#     plt.grid(True)
#     plt.title(title)
#
#     if log_scale:
#         plt.yscale("log")
#
#     return fig


def train_test_split_custom(data, test_size=0.2):
    """
    Returns data split into train and test. Allows for the case with upsampling where we must avoid having the
    same data in both train and test.
    :param data: dataframe
    :param test_size: value between 0 and 1
    :return: train_x, test_x, train_y, test_y where y is the response and x includes all columns except for y
    """
    from sklearn.model_selection import train_test_split
    import pandas as pd
    unique_indices = data[['index', 'target']].drop_duplicates()
    train_temp, test_temp = train_test_split(unique_indices, test_size=test_size, stratify=unique_indices['target'])
    train_temp = train_temp['index']
    test_temp = test_temp['index']
    train = pd.merge(train_temp, data, on='index', how='inner')
    test = pd.merge(test_temp, data, on='index', how='inner')
    train_y, test_y = train['target'], test['target']
    train_x = train.drop(columns=['target', 'index'])
    test_x = test.drop(columns=['target', 'index'])
    return train_x, test_x, train_y, test_y


def train_models(data_input, load_saved=False):
    """
    Returns trained models and split data
    :param data_input: dataframe wih prepared data
    :param load_saved: boolean. If True then load a presaved model else train from data
    :return: models dictionary and data tuple
    """
    train_x, test_x, train_y, test_y = train_test_split_custom(data_input, test_size=0.2)

    if load_saved:
        models = load_models()
    else:
        from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
        from sklearn.linear_model import LogisticRegression

        logistic = LogisticRegression(random_state=42)
        rf = RandomForestClassifier(random_state=42)
        gbm = GradientBoostingClassifier(random_state=42)

        logistic.fit(train_x, train_y)
        rf.fit(train_x, train_y)
        gbm.fit(train_x, train_y)

        save_models(logistic, rf, gbm)

        models = {
            'logistic': logistic,
            'rf': rf,
            'gbm': gbm
        }

    data = (
        train_x,
        test_x,
        train_y,
        test_y
    )
    return models, data


def run_predictions(models, data):
    """
    Returns the test and train data with added columns including the predictions
    :param models: a dictionary with trained models
    :param data: test and train data
    :return: test and train data
    """
    train_x, test_x, train_y, test_y = data
    feature_columns = train_x.columns

    train_x['predicted_rf'] = models['rf'].predict(train_x[feature_columns])
    test_x['predicted_rf'] = models['rf'].predict(test_x[feature_columns])
    train_x['predicted_logistic'] = models['logistic'].predict(train_x[feature_columns])
    test_x['predicted_logistic'] = models['logistic'].predict(test_x[feature_columns])
    train_x['predicted_gbm'] = models['gbm'].predict(train_x[feature_columns])
    test_x['predicted_gbm'] = models['gbm'].predict(test_x[feature_columns])
    return train_x, test_x, train_y, test_y


def diagnostics(train_x, test_x, train_y, test_y):
    """
    Returns a dictionary with AUC values for test and train datasets as well as a report with other metrics
    :param train_x: training data excluding the response
    :param test_x: testing data excluding the response
    :param train_y: training data response only
    :param test_y: test data response only
    :return: metrics dictionary
    """
    from sklearn.metrics import roc_auc_score, classification_report
    metrics = {}
    auc_value_train = roc_auc_score(train_y, train_x['predicted_logistic'])
    auc_value_test = roc_auc_score(test_y, test_x['predicted_logistic'])
    metrics['auc_logistic'] = (auc_value_train, auc_value_test)

    auc_value_train = roc_auc_score(train_y, train_x['predicted_rf'])
    auc_value_test = roc_auc_score(test_y, test_x['predicted_rf'])
    metrics['auc_rf'] = (auc_value_train, auc_value_test)

    auc_value_train = roc_auc_score(train_y, train_x['predicted_gbm'])
    auc_value_test = roc_auc_score(test_y, test_x['predicted_gbm'])
    metrics['auc_gbm'] = (auc_value_train, auc_value_test)

    metrics['report_logistic'] = classification_report(test_y, test_x['predicted_logistic'])
    metrics['report_rf'] = classification_report(test_y, test_x['predicted_rf'])
    metrics['report_gbm'] = classification_report(test_y, test_x['predicted_gbm'])
    return metrics


def run_process(data_input, load_saved=True):
    models, data = train_models(data_input, load_saved)
    metrics = diagnostics(*run_predictions(models, data))
    return metrics, models, data


def prep_data(data):
    # target is the response variable
    # data is upsampled to get the response rate between 10% and 20%.
    import pandas as pd
    data.rename(columns={'Bankrupt?': 'target'}, inplace=True)
    data.sort_values(by='target', inplace=True)
    data.reset_index(inplace=True)

    # upsample data by replicating target equals 1
    data_1 = data.loc[data.target == 1, :]
    data_upsampled = pd.concat([data, data_1, data_1, data_1, data_1], axis=0)
    return data, data_upsampled


def save_models(logistic, rf, gbm):
    import pickle
    with open('outputs/model_files/logistic_model.pickle', 'wb') as f:
        pickle.dump(logistic, f)
    with open('outputs/model_files/rf_model.pickle', 'wb') as f:
        pickle.dump(rf, f)
    with open('outputs/model_files/gbm_model.pickle', 'wb') as f:
        pickle.dump(gbm, f)


def load_models():
    import pickle
    with open('outputs/model_files/logistic_model.pickle', 'rb') as f:
        logistic = pickle.load(f)
    with open('outputs/model_files/rf_model.pickle', 'rb') as f:
        rf = pickle.load(f)
    with open('outputs/model_files/gbm_model.pickle', 'rb') as f:
        gbm = pickle.load(f)

    models = {
        'logistic': logistic,
        'rf': rf,
        'gbm': gbm
    }
    return models
