from model.utils import *
import pandas as pd

data_raw = pd.read_csv("data.csv")
data_raw.drop(columns=[' Current Liabilities/Liability', ' Current Liabilities/Equity'], inplace=True)

data, data_upsampled = prep_data(data_raw)
metrics, models, data_tuple = run_process(data)
metrics_upsampled, _, _ = run_process(data_upsampled)

print('Logistic Regression')
print(metrics['report_logistic'])
print('AUC train test:', metrics['auc_logistic'])

print('Random Forest')
print(metrics['report_rf'])
print('AUC train test:', metrics['auc_rf'])

print('GBM')
print(metrics['report_gbm'])
print('AUC train test:', metrics['auc_gbm'])

print('*' * 30)
print('Upsampled')
print('Logistic Regression')
print(metrics_upsampled['report_logistic'])
print('AUC train test:', metrics_upsampled['auc_logistic'])

print('Random Forest')
print(metrics_upsampled['report_rf'])
print('AUC train test:', metrics_upsampled['auc_rf'])

print('GBM')
print(metrics_upsampled['report_gbm'])
print('AUC train test:', metrics_upsampled['auc_gbm'])

# roc curves
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt

_, test_x, _, test_y = data_tuple

for model, color in zip(models.keys(), ['g', 'b', 'gray']):
    predictions_prob = test_x['predicted_' + model]
    false_positive_rate, recall, thresholds = roc_curve(test_y, predictions_prob)
    roc_auc = auc(false_positive_rate, recall)
    plt.plot(false_positive_rate, recall, color, label='AUC %s = %0.2f' % (model, roc_auc))
    plt.plot([0, 1], [0, 1], 'r--')
    plt.legend(loc='lower right')
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.title('ROC Curve')

# feature importances

feature_columns = [col for col in test_x.columns if not col.startswith('predicted')]

feature_importance = pd.DataFrame(
    {'feature': feature_columns, 'importance': models['rf'].feature_importances_})
feature_importance = feature_importance.sort_values('importance', ascending=False)
top_feature_importance = feature_importance.iloc[0, 1] * 1.0
feature_importance.importance /= top_feature_importance
print(feature_importance)

feature_importance_top_20 = feature_importance.iloc[:20,:]
fig, ax = plt.subplots()
ax.barh(feature_importance_top_20.feature, feature_importance_top_20.importance)
ax.invert_yaxis()
plt.subplots_adjust(left=0.4, right=0.9, bottom=0.1, top=0.9)
ax.set_xlabel('Importance')
ax.set_title('Feature Importance (relative to most important)')
plt.show()